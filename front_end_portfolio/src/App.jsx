import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Navbar from './components/Navbar';
import HomePage from './components/Homepage';
import CarCarPage from './components/CarCarPage';
import ConferenceGoPage from './components/ConferenceGoPage';
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <Router basename="/front_end_portfolio">
      <div className="App">
        <Navbar />
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/car-car" element={<CarCarPage />} />
          <Route path="/conference-go" element={<ConferenceGoPage />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
