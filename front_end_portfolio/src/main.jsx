import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.jsx'; // Make sure this points to your App component
import './index.css'; // Ensure this CSS file exists and is correctly referenced

// Assuming you're using React 18 or later
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
