import React from 'react';
import { Link } from 'react-router-dom';
import '/src/App.css'; // Assuming your styles are here

function Navbar() {
    return (
        <nav style={{
            display: 'flex',
            justifyContent: 'space-around',
            alignItems: 'center',
            padding: '1rem',
            backgroundColor: '#f8f9fa', // A light background for the navbar
            borderBottom: '2px solid #dee2e6', // Border line to separate from the main content
            marginBottom: '1rem',
        }}>
            <ul style={{
                listStyleType: 'none',
                display: 'flex',
                justifyContent: 'space-around',
                alignItems: 'center',
                padding: 0,
                margin: 0,
                width: '100%', // Make the navigation items spread across the navbar
            }}>
                <li style={{ margin: '0 1rem' }}>
                    <Link to="/" style={{ textDecoration: 'none', color: '#007bff', fontSize: '1rem' }}>Home</Link>
                </li>
                <li style={{ margin: '0 1rem' }}>
                    <Link to="/car-car" style={{ textDecoration: 'none', color: '#007bff', fontSize: '1rem' }}>Car Car</Link>
                </li>
                <li style={{ margin: '0 1rem' }}>
                    <Link to="/conference-go" style={{ textDecoration: 'none', color: '#007bff', fontSize: '1rem' }}>Conference-GO</Link>
                </li>
            </ul>
        </nav>
    );
}

export default Navbar;
