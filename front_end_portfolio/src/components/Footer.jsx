import React from 'react';

function Footer() {
    return (
        <footer className="read-the-docs">
            © 2024 Your Company Name
        </footer>
    );
}

export default Footer;
