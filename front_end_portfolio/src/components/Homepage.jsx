import React from 'react';
import '/src/App.css';

function HomePage() {
    return (
        <div className="home-container">
            <div className="intro-section">
                <img src="public/assets/IMG_3912.jpeg" alt="Andrew Brewer" className="logo" />
                <h1>Andrew Brewer</h1>
                <p>Software Developer & Data Scientist</p>
            </div>
            <div className="about-me">
                <p>
                    With a foundation in <strong>Python, JavaScript, Django,</strong> and <strong>React,</strong>
                    I've spent the last few years honing my skills in software development and data analysis.
                    My journey has been marked by a relentless pursuit of knowledge, a passion for solving complex
                    problems, and a commitment to creating impactful, user-centric applications.
                </p>
                <p>
                    Beyond code, my fascination with data science drives me. I am captivated by the power of data
                    to uncover insights, inform decisions, and shape the future. My goal is to merge my development
                    expertise with data science to unlock new possibilities and contribute to innovative, data-driven solutions.
                </p>
                <p>
                    I am currently seeking opportunities that will challenge me, allow me to explore the depths of data science,
                    and leverage my background in software development to make a meaningful impact. Let's connect and explore
                    how we can collaborate on this exciting journey.
                </p>
            </div>
            <div className="contact-me">
                <p>Feel free to reach out via <a href="mailto:andrew.brewer.422@gmail.com">email</a> or connect with me on
                <a href="https://www.linkedin.com/in/andrew-brewer-9864b6219/" target="_blank" rel="noopener noreferrer"> LinkedIn</a>.</p>
            </div>
        </div>
    );
}

export default HomePage;
